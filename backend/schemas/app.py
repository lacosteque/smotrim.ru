from pydantic.dataclasses import dataclass
from pydantic import BaseModel, HttpUrl, validator, Field
from exceptions import BadDomain
from core.config import settings
from typing import Literal

class UserURL(BaseModel):
    url: HttpUrl

    @validator('url')
    def check_domain(cls, value):
        domain = settings.DOMAIN
        if domain not in value:
            raise BadDomain
        return value
    
class ResponseAPI(BaseModel):
    time: str
    status: int
    errors: str
    data: dict[str, dict|str|list|None]


class VideoData(BaseModel):
    status: int
    errors: str
    title: str = None
    anons: str = None
    videos: dict[str, HttpUrl] = None
    picture: HttpUrl = None


