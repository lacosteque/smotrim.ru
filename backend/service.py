from utils.app import get_video_id
from utils.app import parse_api_response
from utils.api import get_api_data
from utils.validation import validation_user_url
from utils.validation import validation_api_response


async def get_data(data: dict):
    video_url = await validation_user_url(data)
    video_id = await get_video_id(video_url)
    api_response = await get_api_data(video_id)
    api_data = await validation_api_response(api_response)
    output = await parse_api_response(api_data)
    return output
