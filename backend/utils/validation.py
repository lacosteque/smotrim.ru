import json
from pydantic import BaseModel, HttpUrl, ValidationError
from schemas.app import UserURL
from schemas.app import ResponseAPI
from exceptions import BadDomain
from core.config import settings
import httpx

async def validation_user_url(data: dict) -> str:
    try:
        user_url = UserURL(**data)
        output = user_url.url
        return output
    except ValidationError as e:
        print(e.json())
    except BadDomain as e:
        print({'status':'', 'error': e})
        exit(1)
        return {'status':'', 'error': e}


async def validation_api_response(data: httpx.Response):
    try:
        output = ResponseAPI(**data.json())
        return output
    except ValidationError as e:
        return {'status':'', 'error': e.json()}
