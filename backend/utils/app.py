import re
from schemas.app import ResponseAPI
from schemas.app import VideoData

async def get_video_id(user_url: str) -> str:
    video_id = re.findall(r'https://smotrim\.ru/video/(\d*)', user_url)
    if video_id:
        out = video_id[0]
        return out

async def parse_api_response(data: ResponseAPI):
    status = data.status
    error = data.errors
    match status:
        case 200:
            path = data.data.get('playlist').get('medialist')[0]
            anons = path.get('anons')
            title = path.get('title')
            videos = path.get('sources').get('http')
            picture = path.get('pictures').get('16:9')

            data = {
                'status': status,
                'errors': error,
                'anons': anons, 
                'title': title, 
                'videos': videos, 
                'picture': picture,
                }
        case _:
            data = {'status': status,
                    'errors': error,
                    }
    output = VideoData(**data)
    return output


