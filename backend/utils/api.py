import httpx

async def get_api_data(video_id: str):
    url = f'https://player.smotrim.ru/iframe/datavideo/id/{video_id}/sid/smotrim'
    async with httpx.AsyncClient(http2=True, timeout=30) as client:
        request = await client.get(url)
        return request
