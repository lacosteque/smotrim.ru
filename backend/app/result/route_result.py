from fastapi import Request, Response
from fastapi import APIRouter
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from service import get_data

templates = Jinja2Templates(directory="templates")
router = APIRouter(include_in_schema=False)

@router.post("/search/")
async def search(request: Request):
    body = await request.json()
    result = await get_data(body)

    match result.status:
        case 200:
            return templates.TemplateResponse(
                    'components/result.html', {'request': request, 'result': result})
        case _:
            return templates.TemplateResponse(
                    'components/error.html', {'request': request, 'result': result})
