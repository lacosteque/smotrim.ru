from fastapi import APIRouter
from app.homepage import route_homepage
from app.result import route_result
from api.v1 import route_result as api_route_result


api_router = APIRouter()
api_router.include_router(route_homepage.router, prefix="", tags=["home-webapp"])
api_router.include_router(route_result.router, prefix="", tags=["result-webapp"])
api_router.include_router(api_route_result.router, prefix="", tags=["result-api"])
