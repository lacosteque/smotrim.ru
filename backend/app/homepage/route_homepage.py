from fastapi import Request
from fastapi import APIRouter
from fastapi.templating import Jinja2Templates


templates = Jinja2Templates(directory="templates")
router = APIRouter(include_in_schema=False)

@router.get("/")
async def homepage(request: Request):
    return templates.TemplateResponse(
        "homepage/homepage.html", {"request": request}
        )


    
