import json
from fastapi import Request, Response
from fastapi import APIRouter
from fastapi.responses import HTMLResponse
from service import get_data

router = APIRouter(include_in_schema=False)

@router.post("/v1/search/")
async def search(request: Request):
    body = await request.json()
    result = await get_data(body)
    data = {
                'anons': result.anons, 
                'title': result.title, 
                'videos': result.videos, 
                'picture': result.picture,
                }


    output = json.dumps(data, ensure_ascii=False)
    return output
        
