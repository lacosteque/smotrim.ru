$(function () {
    $("#form").submit(function (e) {
        e.preventDefault();
        var value = $("#url").val();
        var data = {
            url : value,
        };
        var json = JSON.stringify(data);

        $.ajax({
            url: $(this).attr("action"),
            type: $(this).attr("method"),
            data: json,
            dataType: "html",
            beforeSend: function (xhr, settings) {
                $(".loader").css("visibility", "inherit");
            },
        })
            .done(function (result) {
                $(".loader").css("visibility", "hidden");
                $("#result").html(result);
            })
            .fail(function (xhr, ajaxOptions, thrownError) {
                console.log(arguments);
            });
    });
});

